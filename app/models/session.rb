class Session < ApplicationRecord
  validates :title, :date, :track_id, :presence => true

  has_many :attendances
  has_many :users, through: :attendances

	has_many :questions, dependent: :nullify
	has_many :replies ,through: :questions
	belongs_to :track
end
