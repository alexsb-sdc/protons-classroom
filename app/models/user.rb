class User < ApplicationRecord
  # Include default devise modules.
  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable, :validatable,
          :confirmable, :omniauthable
  include DeviseTokenAuth::Concerns::User
  validates :first_name, :last_name, :email, :gender, :age, :birthdate, :role, :presence => true

  has_many :attendances
  has_many :sessions, through: :attendances

  has_many :questions, dependent: :nullify
  has_many :replies, dependent: :nullify
  has_many :announcements, dependent: :nullify
  has_many :submissions
  belongs_to :track

  def attended_sessions
    self.attendances.length
  end
end
