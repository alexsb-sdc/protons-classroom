class Question < ApplicationRecord
	validates :title, :content, :presence => true

	has_many :replies, dependent: :destroy
	belongs_to :user
	belongs_to :track
	# belongs_to :session
end
