class Track < ApplicationRecord
	validates :title, :year, :presence => true

	has_many :users, dependent: :nullify
	has_many :sessions, dependent: :nullify
	has_many :announcements, dependent: :nullify
	has_many :assignments
end

