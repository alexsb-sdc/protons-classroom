class AnnouncementsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_announcement, only: [:show, :update, :destroy]


  # GET /tracks/:track_id/announcement
  def index
    @announcements = Announcement.where(track_id: params[:track_id]).or(Announcement.where(track_id: nil)).order(id: :desc)
                         .page(params[:pages]).per(params[:count])
    render json: @announcements, include: {:user => {:only => [:first_name, :last_name]}}
  end

  # POST /tracks/:track_id/announcement
  def create
    begin
      @announcement = Announcement.create!(announcement_params)
      render json: @announcement, status: :created
    rescue ActiveRecord::RecordInvalid
      render status: :unprocessable_entity
    end

  end

  # PUT /tracks/:track_id/announcements/:id
  def update
    if not @announcement.nil?
      @announcement.update(announcement_params)
      head :no_content
    else
      render json: {error: "Announcement not found"}, status: :not_found
    end
  end

  # DELETE /tracks/:track_id/announcements/:id
  def destroy
    if not @announcement.nil?
      @announcement.destroy
      head :no_content
    else
      render json: {error: "Announcement not found"}, status: :not_found
    end
  end

  # GET /tracks/:track_id/announcements/:id
  def show
    if not @announcement.nil?
      render json: @announcement, status: :ok
    else
      render json: {error: "Announcement not found"}, status: :not_found
    end
  end


  private
  def announcement_params
    params.permit(:content, :user_id, :track_id)
  end

  def set_announcement
    begin
      @announcement = Announcement.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      @announcement = nil
    end
  end
end
