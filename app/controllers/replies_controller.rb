class RepliesController < ApplicationController
  before_action :authenticate_user!
	before_action :set_reply, only: [:show, :update, :destroy]

	# GET /questions/:question_id/replies
	def index
		if params[:question_id].present?
			@replies = Reply.joins(:user).where(question_id: params[:question_id]).order(created_at: :asc).select(
				"replies.id, content, user_id, question_id,
				 users.first_name as first_name, users.last_name as last_name")
			render json: @replies.page(params[:pages]).per(params[:count]), status: :ok
		else
			render json: {error: "ID of question must be specified"}, status: :unprocessable_entity
		end
	end

	# GET /questions/:question_id/replies/:id
	def show
		if not @reply.nil?
			render json: @reply, status: :ok
		else
			render status: :not_found
		end
	end

	# POST /questions/:question_id/replies
	def create
		begin
			@reply = Reply.create!(replies_params)
			render json: @reply, status: :created
		rescue ActiveRecord::RecordInvalid
			render status: :unprocessable_entity
		end
	end

	# DELETE /questions/:question_id/replies/:id
	def destroy
		if not @reply.nil?
			@reply.destroy
			head :no_content
		else
			render status: :not_found
		end
	end

	# PATCH /questions/:question_id/replies/:id
	def update
		if not @reply.nil?
			@reply.update(replies_params)
			head :no_content
		else
			render status: :not_found
		end
	end


	private
	def replies_params
		params.permit(:content, :question_id, :user_id)
	end

	def set_reply
		begin
			@reply = Reply.find(params[:id])
		rescue ActiveRecord::RecordNotFound
			@reply = nil
		end
	end
end
