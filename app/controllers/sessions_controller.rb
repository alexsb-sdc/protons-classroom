class SessionsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_session, only: [:show, :update, :destroy, :questions]

  # GET /tracks/:track_id/sessions
  def index
    @sessions = Session.where(track_id: params[:track_id]).order(date: :desc).page(params[:pages]).per(params[:count])
    render json: @sessions, status: :ok, include: [:questions => {only: :id}]
  end

  # GET /tracks/:track_id/sessions/1
  def show
    render json: @session, status: :ok
  end


  # POST /sessions
  def create
    @session = Session.new(session_params)
    if @session.save
      render json: @session, status: :ok
    else
      render json: @session.errors, status: 400
    end
  end

  # PUT /sessions/1
  def update
    if @session.update(session_params)
      render json: @session, status: :ok
    else
      render json: @session.errors, status: 400
    end
  end

  # DELETE /sessions/1
  def destroy
    render json: @session.destroy, status: :ok
  end

  # GET /sessions/1/questions
  def questions
    questions = @session.questions
    render json: questions, include: [:user, :replies => {:include => [:user]}]
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_session
    @session = Session.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def session_params
    params.require(:session).permit(:id, :title, :description, :link, :track_id, :date)
  end

end
