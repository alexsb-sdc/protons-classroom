class AssignmentsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_assignment, only: [:show, :update, :destroy]

  # GET /tracks/:track_id/assignments
  def index
    @assignments = Assignment.where(track_id: params[:track_id]).order(deadline: :asc).page(params[:pages]).per(params[:count])
    render json: @assignments
  end

  # GET /assignments/1
  def show
    if params[:user_id]
      subs = @assignment.submissions.where(user_id: params[:user_id])
      render json: @assignment.attributes.merge(submissions: subs)
    else
      render json: @assignment
    end
  end

  # POST /assignments
  def create
    @assignment = Assignment.new(assignment_params)

    if @assignment.save
      render json: @assignment, status: :created, location: @assignment
    else
      render json: @assignment.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /assignments/1
  def update
    if @assignment.update(assignment_params)
      render json: @assignment
    else
      render json: @assignment.errors, status: :unprocessable_entity
    end
  end

  # DELETE /assignments/1
  def destroy
    @assignment.destroy
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_assignment
    @assignment = Assignment.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def assignment_params
    params.require(:assignment).permit(:title, :link, :track, :deadline)
  end
end
