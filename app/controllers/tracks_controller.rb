class TracksController < ApplicationController
  before_action :authenticate_user!
	before_action :set_track, only: [:show, :update, :destroy]

	def index
		@tracks = Track.all()
		render json: @tracks, status: :ok
	end

	def show
		if not @track.nil?
			render json: @track, status: :ok
		else
			render status: :not_found
		end
	end

	def update
		if not @track.nil?
			@track.update(tracks_params)
			head :no_content
		else
			render json: {error: "Track not found"}, status: :not_found
		end
	end

	def destroy
		if not @track.nil?
			@track.destroy
			head :no_content
		else
			render json: {error: "Track not found"}, status: :not_found
		end
	end

	def create
		begin
			@track = Track.create!(tracks_params)
			render json: @track, status: :created
		rescue ActiveRecord::RecordInvalid
			render status: :unprocessable_entity
		end
	end



	private
	def tracks_params
		params.permit(:title, :year)
	end

	def set_track
		begin
			@track = Track.find(params[:id])
		rescue ActiveRecord::RecordNotFound
			@track = nil
		end
	end
end
