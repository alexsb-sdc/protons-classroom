class UsersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_user, only: [:questions, :attended_sessions, :show, :update, :submissions, :unsubmitted_assignments]
  before_action :define_pages_and_counts, only: [:submissions, :unsubmitted_assignments]

  def index
    @users = if params[:track_id]
               User.where(track_id: params[:track_id])
             else
               User.all
             end
    @users = @users.page(params[:pages]).per(params[:count])
    render json: @users, status: :ok
  end

  def create
    @user = User.new(user_params)
    if @user.save
      render json: @user, status: :ok
    else
      render json: @user.errors, status: 400
    end
  end

  def show
    render json: @user, include: [:track]
  end

  def update
    if @user.update(user_params)
      render json: @user, status: :ok
    else
      render json: {message: 'An internal error occurred'}, status: 500
    end
  end

  def attended_sessions
    render json: @user.attended_sessions, status: :ok
  end

  def submissions
    subs = @user.submissions.page(params[:pages]).per(params[:count])
    render json: subs, status: :ok
  end

  def unsubmitted_assignments
    submitted_assignments_ids = @user.submissions.map(&:assignment_id).join(',')
    submitted_assignments_ids = '""' if submitted_assignments_ids == ''
    if params[:track_id]
      assignments = Assignment.where(track_id: params[:track_id]).where('id NOT IN (' + submitted_assignments_ids + ')').page(params[:pages]).per(params[:count])
    else
      assignments = Assignment.where('id NOT IN (' + submitted_assignments_ids + ')').page(params[:pages]).per(params[:count])
    end
    render json: assignments, status: :ok
  end

  def questions
    render json: @user.questions, include: [replies: { include: :user }]
  end

  private

  def set_user
    @user = User.find_by_id(params[:id])
    render json: {message: 'User not found'}, status: 404 unless @user
  end

  def user_params
    params.require(:user).permit(:id, :first_name, :last_name, :email, :gender, :age, :birthdate, :role, :track_id, :bio)
  end
end
