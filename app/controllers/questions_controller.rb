class QuestionsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_question, only: [:show, :update, :destroy]

  # GET /tracks/:track_id/questions
  def index
    if params[:session_id].present? and params[:user_id].present?
      @questions = Question.joins(:user).joins(:session).where(session_id: params[:session_id],
                                                               user_id: params[:user_id], track_id: params[:track_id]).order(id: :desc)
    elsif params[:session_id].present?
      @questions = Question.joins(:session).where(session_id: params[:session_id]).where(track_id: params[:track_id])
                       .order(id: :desc)
    elsif params[:user_id].present?
      @questions = Question.joins(:user).where(user_id: params[:user_id], track_id: params[:track_id], session_id: nil)
                       .order(id: :desc)
    else
      @questions = Question.joins(:user).where(track_id: params[:track_id], session_id: nil).order(id: :desc)
    end
    @questions = @questions.select("questions.id, questions.title, questions.content, questions.updated_at,
										questions.created_at, session_id, users.id as user_id,
										users.first_name as first_name, users.last_name as last_name")
    render json: @questions.page(params[:pages]).per(params[:count]), include: [:user, :replies => {:include => [:user]}], status: :ok
  end

  # POST /tracks/:track_id/questions
  def create
    begin
      @question = Question.create!(question_params)
      render json: @question, status: :created
    rescue ActiveRecord::RecordInvalid
      render status: :unprocessable_entity
    end

  end

  # PUT /tracks/:track_id/questions/:id
  def update
    if not @question.nil?
      @question.update(question_params)
      head :no_content
    else
      render json: {error: "Question not found"}, status: :not_found
    end
  end

  # DELETE /tracks/:track_id/questions/:id
  def destroy
    if not @question.nil?
      @question.destroy
      head :no_content
    else
      render json: {error: "Question not found"}, status: :not_found
    end
  end

  # GET /tracks/:track_id/questions/:id
  def show
    if not @question.nil?
      render json: @question, status: :ok
    else
      render json: {error: "Question not found"}, status: :not_found
    end
  end


  private
  def question_params
    params.permit(:title, :content, :session_id, :user_id, :track_id)
  end

  def set_question
    begin
      @question = Question.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      @question = nil
    end
  end

end
