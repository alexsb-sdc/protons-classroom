class ApplicationController < ActionController::API
  include DeviseTokenAuth::Concerns::SetUserByToken
  before_action :define_pages_and_counts, only: [:index]

  def define_pages_and_counts
      if not params[:pages]
        params[:count] ||= 1000
      else 
        params[:count] ||= 10
      end
  end
end
