def paginate_index(model_name)
  get model_name + '(/page/:pages(/count/:count))' => model_name + '#index'
end

Rails.application.routes.draw do
  mount_devise_token_auth_for 'User', at: 'auth'
  mount RailsAdmin::Engine => '/admin/JI45V05BrH$s0P/', as: 'rails_admin'
  resources :submissions, except: [:index]

  resources :users
  paginate_index('users')
  get '/users/:id/attended_sessions' => 'users#attended_sessions'
  get '/users/:id/unsubmitted_assignments(/page/:pages(/count/:count))' => 'users#unsubmitted_assignments'
  get '/users/:id/submissions(/page/:pages(/count/:count))' => 'users#submissions'
  get '/users/:id/questions' => 'users#questions'

  resources :tracks do

    resources :questions do
      resources :replies
      paginate_index('replies')
    end
    paginate_index('questions')

    resources :announcements
    paginate_index('announcements')

    resources :assignments, only: [:index]
    paginate_index('assignments')

    resources :sessions, only: [:index]
    paginate_index('sessions')

    resources :users
    paginate_index('users')
    get '/users/:id/unsubmitted_assignments(/page/:pages(/count/:count))' => 'users#unsubmitted_assignments'

    resources :submissions, only: [:index]
    paginate_index('submissions')
  end

  resources :assignments, except: [:index]
  get '/assignments/:id/user/:user_id' => 'assignments#show'

  resources :sessions, except: [:index]
  get 'sessions/:id/questions' => 'sessions#questions'

  resources :replies
  paginate_index('replies')
end