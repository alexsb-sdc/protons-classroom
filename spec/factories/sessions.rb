FactoryBot.define do
  factory :session do
  	title { Faker::Lorem.word }
  	track_id 1
  	date {1.years.ago}
  end
end