FactoryBot.define do
	factory :track do
		id 1
		title {Faker::Lorem.word}
		year {Faker::Number.between(2010, 2020).to_s}
	end
end
