FactoryBot.define do
  	factory :announcement do
		content {Faker::Lorem.paragraph}
		track_id 1
		association :user, factory: :user
	end
end