FactoryBot.define do
  	factory :question do
		title { Faker::Lorem.word }
		content {Faker::Lorem.word}
		track_id 1
		association :user, factory: :user
		association :session, factory: :session
	end
end