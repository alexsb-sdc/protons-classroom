FactoryBot.define do
	factory :user do
		first_name { Faker::StarWars.character }
		last_name { Faker::StarWars.character }
		role { Faker::StarWars.character }
		email {Faker::Lorem.word}
		track_id 1
		gender 1
		age 10
		birthdate {Faker::Date.backward(14)}
	end
end