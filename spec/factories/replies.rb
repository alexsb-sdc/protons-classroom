FactoryBot.define do
  	factory :reply do
	    content {Faker::StarWars.character}
	    association :user, factory: :user
	    association :question, factory: :question
  	end
end