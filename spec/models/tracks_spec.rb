require 'rails_helper'

RSpec.describe Track, type: :model do
	it {should have_many(:users).dependent(:nullify)}
	it {should have_many(:sessions).dependent(:nullify)}

	it {should validate_presence_of(:title)}
	it {should validate_presence_of(:year)}
end