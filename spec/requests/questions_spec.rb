require 'rails_helper'


RSpec.describe 'Questions API', type: :request do
	let!(:track) { create(:track) }
	let!(:users) { create_list(:user, 5) }
	let!(:sessions) {create_list(:session, 3)}
	let!(:questions) {create_list(:question, 10)}
  	let(:question_id) { questions.first.id }

	describe 'GET /tracks/:track_id/questions' do
		before {get '/tracks/'+track.id.to_s+'/questions'}

		it 'returns questions' do
			count=0
			questions.each do |q|
				if q.track_id = track.id
					count = count + 1
				end
			end
			expect(json.size).to eq(count)
		end

		it 'returns status code 200' do
			expect(response).to have_http_status(200)
		end
	end

	describe 'GET /tracks/:track_id/questions/:id' do
		before { get '/tracks/' + track.id.to_s + '/questions/' + question_id.to_s }

		context 'when the record exists' do
			it 'returns the question' do
				expect(json).not_to be_empty
				expect(json['id']).to eq(question_id)
			end

			it 'returns status code 200' do
				expect(response).to have_http_status(200)
			end
		end

		context 'when the record does not exist' do
			let(:question_id) {100}

			it 'returns status code 404' do
				expect(response).to have_http_status(404)
			end

		end
	end

	describe 'POST /tracks/:track_id/questions' do
		 let(:valid_attributes) { { title: 'Random Question', content: "With Random Contnent", 
		 							user_id: users.first.id, session_id: sessions.first.id} }

		 let(:invalid_attributes) { {title: 'Random Question with no content', user_id: users.first.id,
		 							session_id: sessions.first.id} }

		context 'when the request is valid' do
      		before { post '/tracks/'+track.id.to_s+'/questions', params: valid_attributes }

	    	it 'creates a quetion' do
	        	expect(json['title']).to eq('Random Question')
	      	end

      		it 'returns status code 201' do
        		expect(response).to have_http_status(201)
      		end
    	end

    	context 'when the request is invalid' do
    		before { post '/tracks/'+track.id.to_s+'/questions', params: invalid_attributes }
    		
    		it 'returns status code 422' do
       			expect(response).to have_http_status(422)
      		end
		end
	end

	describe 'PUT /tracks/:track_id/questions/:id' do
		let(:valid_attributes) { { title: 'Updated Random Question'} }

		context 'when the record exists' do
     		
     		context 'when the attributes are valid' do
	     		before { put "/tracks/"+track.id.to_s+"/questions/"+question_id.to_s, params: valid_attributes }

	      		it 'updates the record' do
	        		expect(response.body).to be_empty
	      		end

	      		it 'returns status code 204' do
	        		expect(response).to have_http_status(204)
	      		end
      		end
    	end

    	context 'when the record does not exist' do
    		before { put "/tracks/" + track.id.to_s + "/questions/100", params: valid_attributes}

			it 'returns status code 404' do
				expect(response).to have_http_status(404)
			end
    	end
	end

	describe 'DELETE /tracks/:track_id/questions/:id' do
		context 'when the record exists' do
		    before { delete "/tracks/" + track.id.to_s + "/questions/"+question_id.to_s }

		    it 'returns status code 204' do
		      expect(response).to have_http_status(204)
		    end
		end

		context 'when the record does not exist' do
			before {delete "/tracks/" + track.id.to_s + "/questions/100"}

			it 'returns status code 404' do
				expect(response).to have_http_status(404)
			end
		end
  	end
end