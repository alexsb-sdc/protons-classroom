require 'rails_helper'

RSpec.describe 'Tracks API', type: :request do
	let!(:track) { create(:track) }

	describe 'GET /tracks' do
		before {get '/tracks'}

		it 'returns tracks' do
			
			expect(json).not_to be_empty
			expect(json.size).to eq(1) 
		end

		it 'returns status code 200' do
			expect(response).to have_http_status(200)
		end
	end

	describe 'GET /tracks/:track_id' do
		before {get '/tracks/'+track.id.to_s}
		context 'when the record exists' do
			it 'returns tracks' do
				expect(json).not_to be_empty
				expect(json['id']).to eq(track.id)
			end

			it 'returns status code 200' do
				expect(response).to have_http_status(200)
			end
		end

		context 'when the record does not exist' do
			before {get '/tracks/100'}

			it 'returns status code 404' do
				expect(response).to have_http_status(404)
			end

		end
	end

	describe 'POST /tracks' do
		 let(:valid_attributes) { { title: 'IoT', year: "2018"} }

		 let(:invalid_attributes) { {title: 'IoT with no yaer'} }

		context 'when the request is valid' do
      		before { post '/tracks', params: valid_attributes }

	    	it 'creates a track' do
	        	expect(json['title']).to eq('IoT')
	      	end

      		it 'returns status code 201' do
        		expect(response).to have_http_status(201)
      		end
    	end

    	context 'when the request is invalid' do
    		before { post '/tracks', params: invalid_attributes }
    		
    		it 'returns status code 422' do
       			expect(response).to have_http_status(422)
      		end
		end
    end


    describe 'PUT /tracks/:id' do
		let(:valid_attributes) { { title: 'Updated Random Track'} }

		context 'when the record exists' do
     		
     		context 'when the attributes are valid' do
	     		before { put "/tracks/"+track.id.to_s, params: valid_attributes }

	      		it 'updates the record' do
	        		expect(response.body).to be_empty
	      		end

	      		it 'returns status code 204' do
	        		expect(response).to have_http_status(204)
	      		end
      		end
    	end

    	context 'when the record does not exist' do
    		before { put "/tracks/100", params: valid_attributes}

			it 'returns status code 404' do
				expect(response).to have_http_status(404)
			end
    	end
	end

	describe 'DELETE /tracks/:id' do
		context 'when the record exists' do
		    before { delete "/tracks/"+track.id.to_s }

		    it 'returns status code 204' do
		      expect(response).to have_http_status(204)
		    end
		end

		context 'when the record does not exist' do
			before {delete "/tracks/100"}

			it 'returns status code 404' do
				expect(response).to have_http_status(404)
			end
		end
  	end

end