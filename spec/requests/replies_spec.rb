require 'rails_helper'

RSpec.describe 'Replies API', type: :request do
	let!(:track) {create(:track)}
	let!(:question) { create(:question) }
	let!(:question_id) {question.id}
	let!(:reply) {create(:reply, question_id: question_id, user_id: question.user_id)}
	let!(:reply_id) {reply.id}

	describe 'GET /tracks/:track_id/questions/:question_id/replies' do
		before {get '/tracks/' + track.id.to_s + '/questions/'+question_id.to_s+'/replies'}

		it 'returns replies' do
			expect(json).not_to be_empty
			expect(json.size).to eq(1) 
		end

		it 'returns status code 200' do
			expect(response).to have_http_status(200)
		end
	end

	describe 'GET /tracks/:track_id/questions/:question_id/replies/:id' do
		before {
			get ('/tracks/' + track.id.to_s + '/questions/'+ question_id.to_s+'/replies/'+reply_id.to_s)
		}

		context 'when the record exists' do
			it 'returns the reply' do
				expect(json).not_to be_empty
				expect(json['id'].to_i).to eq(reply_id)
			end

			it 'returns status code 200' do
				expect(response).to have_http_status(200)
			end
		end

		context 'when the record does not exist' do
			let(:reply_id) {100}

			it 'returns status code 404' do
				expect(response).to have_http_status(404)
			end

		end
	end

	describe 'POST /tracks/:track_id/questions/:question_id/replies' do
		 let(:valid_attributes) { { content: 'Random Reply', question_id: question_id, user_id: question.user_id} }

		 let(:invalid_attributes) { {} }

		context 'when the request is valid' do
      		before { post '/tracks/'+track.id.to_s+'/questions/'+question_id.to_s+'/replies', params: valid_attributes }

	    	it 'creates a reply' do
	        	expect(json['content']).to eq('Random Reply')
	      	end

      		it 'returns status code 201' do
        		expect(response).to have_http_status(201)
      		end
    	end

    	context 'when the request is invalid' do
    		before { post ('/tracks/' + track.id.to_s + '/questions/'+question_id.to_s+'/replies'), params: invalid_attributes }
    		
    		it 'returns status code 422' do
       			expect(response).to have_http_status(422)
      		end
		end
	end

	describe 'PUT /tracks/:track_id/questions/:question_id/replies/:id' do
		let(:valid_attributes) { { conent: 'Updated Random Reply'} }

		context 'when the record exists' do
     		
     		context 'when the attributes are valid' do
	     		before { put "/tracks/" + track.id.to_s + "/questions/"+question_id.to_s+"/replies/"+reply_id.to_s,\
	     		 params: valid_attributes }

	      		it 'updates the record' do
	        		expect(response.body).to be_empty
	      		end

	      		it 'returns status code 204' do
	        		expect(response).to have_http_status(204)
	      		end
      		end
    	end

    	context 'when the record does not exist' do
    		before { put "/tracks/" + track.id.to_s + "/questions/"+question_id.to_s+"/replies/1111", params: valid_attributes}

			it 'returns status code 404' do
				expect(response).to have_http_status(404)
			end
    	end
	end

	describe 'DELETE /tracks/:track_id/questions/:question_id/replies/:id' do
		context 'when the record exists' do
		    before { delete "/tracks/" + track.id.to_s + "/questions/"+question_id.to_s+"/replies/"+reply_id.to_s }

		    it 'returns status code 204' do
		      expect(response).to have_http_status(204)
		    end
		end

		context 'when the record does not exist' do
			before {delete "/tracks/"+track.id.to_s+"/questions/"+question_id.to_s+"/replies/100" }

			it 'returns status code 404' do
				expect(response).to have_http_status(404)
			end
		end
  	end

end