require 'rails_helper'


RSpec.describe 'Announcemenets API', type: :request do
	let!(:track) { create(:track) }
	let!(:users) { create_list(:user, 5) }
	let!(:announcements) {create_list(:announcement, 10)}
  	let(:announcement_id) { announcements.first.id }

  	describe 'GET /tracks/:track_id/announcements' do
		before {get '/tracks/'+track.id.to_s+'/announcements'}

		it 'returns announcements' do
			count=0
			announcements.each do |a|
				if a.track_id = track.id
					count = count + 1
				end
			end
			expect(json.size).to eq(count)
		end

		it 'returns status code 200' do
			expect(response).to have_http_status(200)
		end
	end

	describe 'GET /tracks/:track_id/announcements/:id' do
		before { get '/tracks/'+track.id.to_s+'/announcements/' + announcement_id.to_s }

		context 'when the record exists' do
			it 'returns the announcement' do
				expect(json).not_to be_empty
				expect(json['id']).to eq(announcement_id)
			end

			it 'returns status code 200' do
				expect(response).to have_http_status(200)
			end
		end

		context 'when the record does not exist' do
			let(:announcement_id) {100}

			it 'returns status code 404' do
				expect(response).to have_http_status(404)
			end

		end
	end

	describe 'POST /tracks/:track_id/announcements' do
		 let(:valid_attributes) { { content: "Random Announcement content", 
		 							user_id: users.first.id} }

		 let(:invalid_attributes) { {content: 'Random Announcement with no author'} }

		context 'when the request is valid' do
      		before { post '/tracks/'+track.id.to_s+'/announcements', params: valid_attributes }

	    	it 'creates an announcement' do
	        	expect(json['content']).to eq('Random Announcement content')
	      	end

      		it 'returns status code 201' do
        		expect(response).to have_http_status(201)
      		end
    	end

    	context 'when the request is invalid' do
    		before { post '/tracks/'+track.id.to_s+'/announcements', params: invalid_attributes }
    		
    		it 'returns status code 422' do
       			expect(response).to have_http_status(422)
      		end
		end
	end

	describe 'PUT /tracks/:track_id/announcements/:id' do
		let(:valid_attributes) { { title: 'Updated Random Announcement'} }

		context 'when the record exists' do
     		
     		context 'when the attributes are valid' do
	     		before { put "/tracks/"+track.id.to_s+"/announcements/"+announcement_id.to_s, params: valid_attributes }

	      		it 'updates the record' do
	        		expect(response.body).to be_empty
	      		end

	      		it 'returns status code 204' do
	        		expect(response).to have_http_status(204)
	      		end
      		end
    	end

    	context 'when the record does not exist' do
    		before { put "/tracks/" + track.id.to_s + "/announcements/100", params: valid_attributes}

			it 'returns status code 404' do
				expect(response).to have_http_status(404)
			end
    	end
	end

	describe 'DELETE /tracks/:track_id/announcements/:id' do
		context 'when the record exists' do
		    before { delete "/tracks/" + track.id.to_s + "/announcements/"+announcement_id.to_s }

		    it 'returns status code 204' do
		      expect(response).to have_http_status(204)
		    end
		end

		context 'when the record does not exist' do
			before {delete "/tracks/" + track.id.to_s + "/announcements/100"}

			it 'returns status code 404' do
				expect(response).to have_http_status(404)
			end
		end
  	end

end