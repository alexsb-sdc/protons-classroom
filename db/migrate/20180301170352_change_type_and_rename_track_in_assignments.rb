class ChangeTypeAndRenameTrackInAssignments < ActiveRecord::Migration[5.1]
  def change
    remove_column :assignments, :track
    add_column :assignments, :track_id, :integer
  end
end
