class ChangeNullConstraintInAnnouncements < ActiveRecord::Migration[5.1]
  def change
    change_column :announcements, :track_id, :integer, null: true
  end
end
