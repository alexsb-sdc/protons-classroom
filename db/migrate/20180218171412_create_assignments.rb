class CreateAssignments < ActiveRecord::Migration[5.1]
  def change
    create_table :assignments do |t|
      t.string :title
      t.string :link
      t.string :track
      t.datetime :deadline

      t.timestamps
    end
  end
end
