class CreateReplies < ActiveRecord::Migration[5.1]
  def change
    create_table :replies do |t|
      t.text :content, null: false
      t.bigint :user_id, null: false
      t.bigint :question_id, null: false

      t.timestamps
    end
  end
end
