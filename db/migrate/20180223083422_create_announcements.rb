class CreateAnnouncements < ActiveRecord::Migration[5.1]
  def change
    create_table :announcements do |t|
    	t.text :content, null: false
    	t.bigint :track_id, null: false
    	t.bigint :user_id, null: false
      	t.timestamps
    end
  end
end
