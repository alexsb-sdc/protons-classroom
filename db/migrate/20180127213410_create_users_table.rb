class CreateUsersTable < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :first_name, null: false
      t.string :last_name, null: false
      t.string :email, null: false
      t.string :password, null: false
      t.text :bio
      t.string :role
      t.integer :gender, null: false
      t.integer :age, null: false
      t.bigint :track_id
      t.boolean :active
      t.date :birthdate, null: false

      t.timestamps
    end
  end
end
