class AddNullConstraintToTrackInQuestions < ActiveRecord::Migration[5.1]
  def change
  	change_column :questions, :track_id, :bigint, null: true
  end
end
