class ChangeNullConstrainsInQuestions < ActiveRecord::Migration[5.1]
  def up
  	change_column :questions, :session_id, :bigint, null: true
  	change_column :questions, :track_id, :bigint, null: false
  end

  def down
  	change_column :questions, :session_id, :bigint, null: false
  	change_column :questions, :track_id, :bigint, null: true
  end
end
