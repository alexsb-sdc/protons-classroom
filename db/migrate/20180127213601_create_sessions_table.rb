class CreateSessionsTable < ActiveRecord::Migration[5.1]
  def change
    create_table :sessions do |t|
      t.string :title, null: false
      t.string :description
      t.string :link
      t.bigint :track_id, null: false
      t.date :date, null: false

      t.timestamps
    end
  end
end
