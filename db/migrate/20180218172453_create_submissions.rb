class CreateSubmissions < ActiveRecord::Migration[5.1]
  def change
    create_table :submissions do |t|
      t.integer :user_id
      t.integer :assignment_id
      t.string :grade
      t.string :solution
      t.integer :instructor_id
      t.datetime :date
      t.text :comments

      t.timestamps
    end
  end
end
