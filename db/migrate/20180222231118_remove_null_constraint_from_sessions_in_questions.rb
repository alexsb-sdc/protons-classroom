class RemoveNullConstraintFromSessionsInQuestions < ActiveRecord::Migration[5.1]
  def change
  	change_column :questions, :session_id, :bigint, null: false
  end
end
