# tracks creation
programming = Track.create(title: 'Programming', year: '2018')
iot = Track.create(title: 'IoT', year: '2018')
robotic = Track.create(title: 'Robotics', year: '2018')

# # protons creation
# 30.times do |n|
#   n = User.new(first_name: Faker::Name.first_name,
#               last_name: Faker::Name.last_name,
#               email: Faker::Internet.email,
#               role: 'proton',
#               gender: rand(2),
#               track_id: 1 + rand(3),
#               active: true,
#               birthdate: Faker::Date.birthday(10, 17),
#               age: 10 + rand(8),
#               password: '12345678')
#   n.skip_confirmation!
#   n.save
# end
#
# # instructors creation
# 10.times do |n|
#   n = User.create(first_name: Faker::Name.first_name,
#               last_name: Faker::Name.last_name,
#               email: Faker::Internet.email,
#               role: 'instructor',
#               gender: rand(2),
#               track_id: 1 + rand(3),
#               active: true,
#               birthdate: Faker::Date.birthday(18, 24),
#               age: 10 + rand(8),
#               password: '12345678')
#   n.skip_confirmation!
#   n.save
# end

# # sessions creation
# 30.times do |n|
#   Session.create(title: Faker::Movie.quote,
#                  description: Faker::Hacker.say_something_smart,
#                  link: Faker::Internet.url,
#                  track_id: 1 + rand(3),
#                  date: Faker::Date.forward(100))
# end
#
# # assignments creation
# 30.times do |n|
#   Assignment.create(title: Faker::Movie.quote,
#                     link: Faker::Internet.url,
#                     track_id: 1 + rand(3),
#                     deadline: Faker::Date.forward(100))
# end
#
# # submissions creation
# 30.times do |n|
#     Submission.create(user_id: 1+ rand(30),
#                       solution: Faker::Internet.url,
#                       comments: Faker::Hacker.say_something_smart,
#                       grade: 'A',
#                       date: Faker::Date.forward(100),
#                       instructor_id: 30 + rand(10),
#                       assignment_id: rand(31))
# end
#
# # announcements creation
# 10.times do |n|
#     Announcement.create(user_id: 1+ rand(30),
#                       content: Faker::Hacker.say_something_smart,
#                       track_id: 1 + rand(3))
# end
#
#
# # questions creation
# 20.times do |n|
#     Question.create(user_id: 1+ rand(30),
#                       content: Faker::Hacker.say_something_smart,
#                       track_id: 1 + rand(3),
#                       session_id: 1 + rand(30),
#                       title: Faker::Movie.quote)
# end
#
# # forum questions creation
# 10.times do |n|
#   Question.create(user_id: 1+ rand(30),
#                   content: Faker::Hacker.say_something_smart,
#                   track_id: 1 + rand(3),
#                   session_id: nil,
#                   title: Faker::Movie.quote)
# end
#
#
# # replies creation
# 20.times do |n|
#     Reply.create(user_id: 1+ rand(30),
#                       content: Faker::Hacker.say_something_smart,
#                       question_id: 1 + rand(20))
# end

###################
# Believable data #
###################

ali = User.create(first_name: 'Ali',
                  last_name: 'Sherif',
                  email: 'ali@sherif.com',
                  role: 'proton',
                  gender: 1,
                  track_id: 1,
                  active: true,
                  birthdate: Faker::Date.birthday(18, 24),
                  age: 10 + rand(8),
                  password: '12345678')
ali.skip_confirmation!
ali.save


abdo = User.create(first_name: 'Abdulrahman',
                   last_name: 'Suliman',
                   email: 'abdo@suliman.com',
                   role: 'proton',
                   gender: 1,
                   track_id: 1,
                   active: true,
                   birthdate: Faker::Date.birthday(18, 24),
                   age: 10 + rand(8),
                   password: '12345678')
abdo.skip_confirmation!
abdo.save


joe = User.create(first_name: 'Youssef',
                  last_name: 'Fares',
                  email: 'joe@fares.com',
                  role: 'instructor',
                  gender: 1,
                  track_id: 1,
                  active: true,
                  birthdate: Faker::Date.birthday(18, 24),
                  age: 10 + rand(8),
                  password: '12345678')
joe.skip_confirmation!
joe.save


sdc = User.create(first_name: 'Software',
                  last_name: 'Committee',
                  email: 'sdc@alexsb.org',
                  role: 'instructor',
                  gender: 1,
                  track_id: 1,
                  active: true,
                  birthdate: Faker::Date.birthday(18, 24),
                  age: 10 + rand(8),
                  password: '12345678')
sdc.skip_confirmation!
sdc.save

s1 = Session.create(title: 'Session #1',
                    description: 'Introduction to Computer Science in Scratch',
                    link: 'https://www.google.com',
                    track_id: 1,
                    date: '12/03/2018')

s2 = Session.create(title: 'Session #2',
                    description: 'Scratch Continued',
                    link: 'https://www.google.com',
                    track_id: 1,
                    date: '15/03/2018')

s3 = Session.create(title: 'Session #3',
                    description: 'Variables & Conditionals',
                    link: 'https://www.google.com',
                    track_id: 1,
                    date: '19/03/2018')


a1 = Assignment.create(title: 'T-Rex Runner',
                       link: Faker::Internet.url,
                       track_id: 1,
                       deadline: '19/03/2018')

a2 = Assignment.create(title: 'Problem Set #1',
                       link: Faker::Internet.url,
                       track_id: 1,
                       deadline: '26/03/2018')


q1 = Question.create(user_id: abdo.id,
                     content: 'Where can I download scratch?',
                     track_id: 1,
                     session_id: s1.id,
                     title: 'Scratch Download')


q2 = Question.create(user_id: abdo.id,
                     title: '****',
                     track_id: 1,
                     session_id: s2.id,
                     content: 'I don\'t undersand threads in Scratch can someone help me?')


q3 = Question.create(user_id: abdo.id,
                     title: '****',
                     track_id: 1,
                     session_id: nil,
                     content: 'Can I be late to the next session?')


q4 = Question.create(user_id: ali.id,
                     title: '****',
                     track_id: 1,
                     session_id: nil,
                     content: 'I skipped the last session what can I do')


r1 = Reply.create(user_id: joe.id,
                  content: 'here\'s the link: https://scratch.mit.edu/download',
                  question_id: q1.id)


r2 = Reply.create(user_id: joe.id,
                  content: 'Yes, it\'s okay, some technical team member will
                            help you follow up in the session',
                  question_id: q3.id)


Announcement.create(user_id: sdc.id,
                    content: 'Protons Classroom is UP!',
                    track_id: 1)


Announcement.create(user_id: sdc.id,
                    content: 'Welcome to Programming Track',
                    track_id: 1)


Announcement.create(user_id: sdc.id,
                    content: 'Session #1 is uploaded',
                    track_id: 1)


Announcement.create(user_id: sdc.id,
                    content: 'Session #2 is uploaded',
                    track_id: 1)


Announcement.create(user_id: sdc.id,
                    content: 'T-Rex Assignment is uploaded',
                    track_id: 1)


Announcement.create(user_id: sdc.id,
                    content: 'Session #3 is uploaded',
                    track_id: 1)


Announcement.create(user_id: sdc.id,
                    content: 'Problem Set #1 is uploaded',
                    track_id: 1)


u = User.create( birthdate: '1996-12-04' , role: 'proton' , age: '14' , track_id: '2' ,first_name: "Riham",last_name: "Mohamed",email: "Riham.mohamed2510@gmail.com",password: "BvV8EhYx") 
u.skip_confirmation! 
u.save
u = User.create( birthdate: '1996-12-04' , role: 'proton' , age: '14' , track_id: '2' ,first_name: "Amr",last_name: "Mostafa",email: "amr.mostafa582001@gmail.com",password: "yykdO2vQ") 
u.skip_confirmation! 
u.save
u = User.create( birthdate: '1996-12-04' , role: 'proton' , age: '14' , track_id: '2' ,first_name: "Shahd",last_name: "hany",email: "Hanyragab198@hotmail.com",password: "hemSBUS1") 
u.skip_confirmation! 
u.save
u = User.create( birthdate: '1996-12-04' , role: 'proton' , age: '14' , track_id: '2' ,first_name: "Farah",last_name: "Alaa",email: "Farahkhatab@outlook.com",password: "3Zwd0hHs") 
u.skip_confirmation! 
u.save
u = User.create( birthdate: '1996-12-04' , role: 'proton' , age: '14' , track_id: '2' ,first_name: "Nehal",last_name: "osama",email: "nehalossama2001@gmail.com",password: "mmBjmrHe") 
u.skip_confirmation! 
u.save
u = User.create( birthdate: '1996-12-04' , role: 'proton' , age: '14' , track_id: '2' ,first_name: "Loujaina",last_name: "Mahamoud",email: "Loujainashehata@gmail.com",password: "bAYFLhVl") 
u.skip_confirmation! 
u.save
u = User.create( birthdate: '1996-12-04' , role: 'proton' , age: '14' , track_id: '2' ,first_name: "Hania",last_name: "Ragy",email: "CatastropheXD@outlook.com",password: "3nhtKPLc") 
u.skip_confirmation! 
u.save
u = User.create( birthdate: '1996-12-04' , role: 'proton' , age: '14' , track_id: '2' ,first_name: "Belal",last_name: "Mohamed",email: "belal.mohamedkamal@hotmail.com",password: "h6TE8xiZ") 
u.skip_confirmation! 
u.save
u = User.create( birthdate: '1996-12-04' , role: 'proton' , age: '14' , track_id: '2' ,first_name: "Ahmed",last_name: "Tarek",email: "hamada.zaki2002@hotmail.com",password: "rcLWghNq") 
u.skip_confirmation! 
u.save
u = User.create( birthdate: '1996-12-04' , role: 'proton' , age: '14' , track_id: '2' ,first_name: "Yusuf",last_name: "Orief",email: "dr_maymoheb@yahoo.com",password: "m7mN1uag") 
u.skip_confirmation! 
u.save
u = User.create( birthdate: '1996-12-04' , role: 'proton' , age: '14' , track_id: '2' ,first_name: "Hazem",last_name: "mohamed",email: "hazem3hazem1@gmail.com ",password: "opTAssM3") 
u.skip_confirmation! 
u.save
u = User.create( birthdate: '1996-12-04' , role: 'proton' , age: '14' , track_id: '2' ,first_name: "gehad",last_name: "sameh",email: "gehadsameh2001@gmail.com",password: "Z1e2iNp6") 
u.skip_confirmation! 
u.save
u = User.create( birthdate: '1996-12-04' , role: 'proton' , age: '14' , track_id: '2' ,first_name: "معاذ",last_name: "مصطفى",email: "Storescary1@gmail.com",password: "G1BTnaiY") 
u.skip_confirmation! 
u.save
u = User.create( birthdate: '1996-12-04' , role: 'proton' , age: '14' , track_id: '2' ,first_name: "Shahd",last_name: "Mansour",email: "shahd_mansour2005@yahoo.com",password: "zs0rF0Li") 
u.skip_confirmation! 
u.save
u = User.create( birthdate: '1996-12-04' , role: 'proton' , age: '14' , track_id: '2' ,first_name: "
salma",last_name: "Muhamed",email: "salmamohamed_2277@yahoo.com ",password: "4OkYVNuP") 
u.skip_confirmation! 
u.save
u = User.create( birthdate: '1996-12-04' , role: 'proton' , age: '14' , track_id: '2' ,first_name: "Peter",last_name: "Onsi",email: "Peteronsi2016@gmail.com",password: "rjvHdoZP") 
u.skip_confirmation! 
u.save
u = User.create( birthdate: '1996-12-04' , role: 'proton' , age: '14' , track_id: '2' ,first_name: "نور",last_name: "مدحت",email: "Nourmedhat72@gmail.com",password: "Xw3omdWh") 
u.skip_confirmation! 
u.save
u = User.create( birthdate: '1996-12-04' , role: 'proton' , age: '14' , track_id: '2' ,first_name: "Yousef",last_name: "Magdy",email: "yousefelgoharyx@gmail.com",password: "mIW3Ujbi") 
u.skip_confirmation! 
u.save
u = User.create( birthdate: '1996-12-04' , role: 'proton' , age: '14' , track_id: '2' ,first_name: "Salwa",last_name: "Said",email: "esraamoubarek0101@gmail.com",password: "UgKmyml4") 
u.skip_confirmation! 
u.save
u = User.create( birthdate: '1996-12-04' , role: 'proton' , age: '14' , track_id: '2' ,first_name: "Rana",last_name: "Sherif",email: "ranasherif.295@gmail.com",password: "giPPcLQE") 
u.skip_confirmation! 
u.save
u = User.create( birthdate: '1996-12-04' , role: 'proton' , age: '14' , track_id: '2' ,first_name: "abdullah",last_name: "mohamed",email: "abdullahfawzy2005@gmail.com",password: "GrRAabIy") 
u.skip_confirmation! 
u.save
u = User.create( birthdate: '1996-12-04' , role: 'proton' , age: '14' , track_id: '2' ,first_name: "Omar",last_name: "Ashaf",email: "O_Aly@outlook.com",password: "JWy810mD") 
u.skip_confirmation! 
u.save
u = User.create( birthdate: '1996-12-04' , role: 'proton' , age: '14' , track_id: '2' ,first_name: "عمر",last_name: "شريف",email: "ali22222omar@gmail.com",password: "BHzBkQ29") 
u.skip_confirmation! 
u.save
u = User.create( birthdate: '1996-12-04' , role: 'proton' , age: '14' , track_id: '2' ,first_name: "محمد",last_name: "السيد",email: "alsayedm12@gmail.com",password: "Mb9Omfqa") 
u.skip_confirmation! 
u.save
u = User.create( birthdate: '1996-12-04' , role: 'proton' , age: '14' , track_id: '2' ,first_name: "Omar",last_name: "Hesham",email: "omarhk781@gmail.com",password: "b53Obpos") 
u.skip_confirmation! 
u.save
u = User.create( birthdate: '1996-12-04' , role: 'proton' , age: '14' , track_id: '2' ,first_name: "Mohamed",last_name: "Osama",email: "mohamedosama4519@gmail.com",password: "RVjdqA09") 
u.skip_confirmation! 
u.save
u = User.create( birthdate: '1996-12-04' , role: 'proton' , age: '14' , track_id: '2' ,first_name: "Mariam",last_name: "Mahmoud",email: "mariamelshwahy@yahoo.com",password: "Epvk1twK") 
u.skip_confirmation! 
u.save
u = User.create( birthdate: '1996-12-04' , role: 'proton' , age: '14' , track_id: '2' ,first_name: "Abdelrahman",last_name: "Elsonbaty",email: "sonbaty168@gmail.com",password: "KzfJqHmw") 
u.skip_confirmation! 
u.save
u = User.create( birthdate: '1996-12-04' , role: 'proton' , age: '14' , track_id: '2' ,first_name: "Ibrahim",last_name: "Ahmed",email: "midofox2020.ia@gmail.com",password: "s6Hj7OZI") 
u.skip_confirmation! 
u.save
u = User.create( birthdate: '1996-12-04' , role: 'proton' , age: '14' , track_id: '2' ,first_name: "مريم",last_name: "مريم",email: "magdymarym488@gmail.com",password: "Tx5vQcte") 
u.skip_confirmation! 
u.save
u = User.create( birthdate: '1996-12-04' , role: 'proton' , age: '14' , track_id: '2' ,first_name: "Nayrouz",last_name: "Ahmed",email: "Prettynozy@yahoo.com",password: "OZE5f2JZ") 
u.skip_confirmation! 
u.save
u = User.create( birthdate: '1996-12-04' , role: 'proton' , age: '14' , track_id: '2' ,first_name: "Karim",last_name: "Elsamadicy",email: "daliamohebeldin@hotmail.com",password: "Xm9PM9JV") 
u.skip_confirmation! 
u.save
u = User.create( birthdate: '1996-12-04' , role: 'proton' , age: '14' , track_id: '2' ,first_name: "Nouran",last_name: "youssef",email: "Nouranyoussefii@yahoo.com",password: "xveO9xGo") 
u.skip_confirmation! 
u.save
u = User.create( birthdate: '1996-12-04' , role: 'proton' , age: '14' , track_id: '2' ,first_name: "ahmed",last_name: "medhat",email: "hodamoh51@gmail.com",password: "b8oFB2wN") 
u.skip_confirmation! 
u.save
u = User.create( birthdate: '1996-12-04' , role: 'proton' , age: '14' , track_id: '2' ,first_name: "Michael",last_name: "Milad",email: "Michaelmiladkamel@outlook.com",password: "NWh3jbB5") 
u.skip_confirmation! 
u.save








