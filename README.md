# Protons Classroom

A course management system for Protons educational program.

## API
 ```
                      Prefix Verb   URI Pattern                                                    Controller#Action
         static_pages_routes GET    /static_pages/routes(.:format)                                 application#index
                 submissions GET    /submissions(.:format)                                         submissions#index
                             POST   /submissions(.:format)                                         submissions#create
                  submission GET    /submissions/:id(.:format)                                     submissions#show
                             PATCH  /submissions/:id(.:format)                                     submissions#update
                             PUT    /submissions/:id(.:format)                                     submissions#update
                             DELETE /submissions/:id(.:format)                                     submissions#destroy
                       users GET    /users(.:format)                                               users#index
                             POST   /users(.:format)                                               users#create
                        user GET    /users/:id(.:format)                                           users#show
                             PATCH  /users/:id(.:format)                                           users#update
                             PUT    /users/:id(.:format)                                           users#update
                             DELETE /users/:id(.:format)                                           users#destroy
                             GET    /users/:id/attended_sessions(.:format)                         users#attended_sessions
                             GET    /users/:id/submissions(.:format)                               users#submissions
      track_question_replies GET    /tracks/:track_id/questions/:question_id/replies(.:format)     replies#index
                             POST   /tracks/:track_id/questions/:question_id/replies(.:format)     replies#create
        track_question_reply GET    /tracks/:track_id/questions/:question_id/replies/:id(.:format) replies#show
                             PATCH  /tracks/:track_id/questions/:question_id/replies/:id(.:format) replies#update
                             PUT    /tracks/:track_id/questions/:question_id/replies/:id(.:format) replies#update
                             DELETE /tracks/:track_id/questions/:question_id/replies/:id(.:format) replies#destroy
             track_questions GET    /tracks/:track_id/questions(.:format)                          questions#index
                             POST   /tracks/:track_id/questions(.:format)                          questions#create
              track_question GET    /tracks/:track_id/questions/:id(.:format)                      questions#show
                             PATCH  /tracks/:track_id/questions/:id(.:format)                      questions#update
                             PUT    /tracks/:track_id/questions/:id(.:format)                      questions#update
                             DELETE /tracks/:track_id/questions/:id(.:format)                      questions#destroy
         track_announcements GET    /tracks/:track_id/announcements(.:format)                      announcements#index
                             POST   /tracks/:track_id/announcements(.:format)                      announcements#create
          track_announcement GET    /tracks/:track_id/announcements/:id(.:format)                  announcements#show
                             PATCH  /tracks/:track_id/announcements/:id(.:format)                  announcements#update
                             PUT    /tracks/:track_id/announcements/:id(.:format)                  announcements#update
                             DELETE /tracks/:track_id/announcements/:id(.:format)                  announcements#destroy
           track_assignments GET    /tracks/:track_id/assignments(.:format)                        assignments#index
              track_sessions GET    /tracks/:track_id/sessions(.:format)                           sessions#index
                      tracks GET    /tracks(.:format)                                              tracks#index
                             POST   /tracks(.:format)                                              tracks#create
                       track GET    /tracks/:id(.:format)                                          tracks#show
                             PATCH  /tracks/:id(.:format)                                          tracks#update
                             PUT    /tracks/:id(.:format)                                          tracks#update
                             DELETE /tracks/:id(.:format)                                          tracks#destroy
                 assignments POST   /assignments(.:format)                                         assignments#create
                  assignment GET    /assignments/:id(.:format)                                     assignments#show
                             PATCH  /assignments/:id(.:format)                                     assignments#update
                             PUT    /assignments/:id(.:format)                                     assignments#update
                             DELETE /assignments/:id(.:format)                                     assignments#destroy
                    sessions POST   /sessions(.:format)                                            sessions#create
                     session GET    /sessions/:id(.:format)                                        sessions#show
                             PATCH  /sessions/:id(.:format)                                        sessions#update
                             PUT    /sessions/:id(.:format)                                        sessions#update
                             DELETE /sessions/:id(.:format)                                        sessions#destroy
                             GET    /users/:id/unsubmitted_assignments(.:format)                   users#unsubmitted_assignments
```
